package myproject;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class mymethods {
	
	private WebDriver driver;
	private WebDriverWait wait;
	
	public void openWebsite1(String URL) throws InterruptedException {
		//Create prefs map to store all preferences 
		Map<String, Object> prefs = new HashMap<String, Object>();

		//Put this into prefs map to switch off browser notification (2 to disable)
		prefs.put("profile.default_content_setting_values.notifications", 1);

		//Create chrome options to set this prefs
		ChromeOptions options = new ChromeOptions();
		
		options.setExperimentalOption("prefs", prefs);
		System.setProperty("webdriver.chrome.driver","C:\\Selenium Webdrivers\\chromedriver.exe");
		 driver = new ChromeDriver(options);
		 wait = new WebDriverWait(driver,300);
//		 softassert = new SoftAssert();

		driver.get(URL);  
		Thread.sleep(3000);
		driver.findElement(By.xpath("//div[@class='bagdrop bf-cont active']//button[contains(@class,'btn btn-primary')]")).click();
		Thread.sleep(3000);
		driver.manage().window().maximize();
		
		//save the reference to the current window.
		String parentWindow= driver.getWindowHandle();
		//switch to new window
		for (String winHandle : driver.getWindowHandles()) {
		    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		}
	}
}
