package cargoWebApp;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.Status;

import TestData.ReadExcelFile;



public class customer {
	public WebDriver driver;	
	private WebDriverWait wait;
	private SoftAssert softassert;
	static String orName = "/src/main/resources/CargoRepository.json";
	
	@Test (priority = 0)
	public void enterCargoDetails() throws IOException, InterruptedException {
		
		driver.get("https://uat.shyftclub.com/cargo");  
		driver.manage().window().maximize();
		Thread.sleep(3000);
		selectcombobox("Dubai","Origin","Origin Dropdown values");
		selectcombobox("London","Destination","Destination Dropdown values");
		Thread.sleep(3000);
		selectcombobox("Personal Effects","Cargo Type","Cargo Type Dropdown values");
		findelement("Date").click();
		Cargo_setCalenderDate("14 August");
		findelement("Search").click();
		Thread.sleep(3000);
		verifyCargosService("Dubai Cargos","Personal Effects","Standard","Air Freight","Door To Door","1-1 Days","123 Kg","AED 1", "AED 0 per kg");
		Thread.sleep(3000);

	}
	
	@Test (priority = 1)
	public void searchResultsPage() throws IOException, InterruptedException {
//		findelement("Back").click();
		selectcombobox("Dubai","Origin","Origin Dropdown values");
		selectcombobox("Colombo","Destination","Destination Dropdown values");
		selectcombobox("Personal Effects","Cargo Type","Cargo Type Dropdown values");
		findelement("Date").click();
		Cargo_setCalenderDate("14 August");
		findelement("Search").click();
		verifyCargosService("Dubai Cargos","Personal Effects","Standard","Air Freight","Door To Door","1-1 Days","123 Kg","AED 1", "AED 0 per kg");
		Thread.sleep(3000);
	}
	
	@Test (priority = 2)
	public void verifyFastestSorting() throws IOException, InterruptedException {
		//verifyFastestSorting
				findelement("radio_Fastest").click();
				verifyCargosService("Dubai Cargos","Personal Effects","Standard","Air Freight","Door To Door","1-1 Days","","AED 1", "AED 0 per kg");
	}
	
	@Test (priority = 3)
	public void verifyCheapestSorting() throws IOException, InterruptedException {
	//verifyCheapestSorting
	findelement("radio_Cheapest").click();
	String priceRangeMin = findelement("PriceRangeMin").getText();
	System.out.println(priceRangeMin);
	verifyCargosService("Dubai Cargos","Personal Effects","Standard","Air Freight","Door To Door","1-1 Days","",priceRangeMin, "AED 0 per kg");
	}
	
	@Test (priority = 4)
	public void verifySeaFreightFilter() throws IOException, InterruptedException {
	//verifySeaFreight
	findelement("SeaFreightCheckbox").click();
	verifyCargosService("Dubai Cargos","Personal Effects","Standard","Sea Freight","Door To Door","1-1 Days","","AED 1", "AED 0 per kg");
	findelement("SeaFreightCheckbox").click();
	}
	
	@Test (priority = 5)
	public void verifyAirFreightFilter() throws IOException, InterruptedException {
	//verifyAirFreight		
	findelement("AirFreightCheckbox").click();
	verifyCargosService("Dubai Cargos","Personal Effects","Standard","Air Freight","Door To Door","1-1 Days","","AED 1", "AED 0 per kg");
	findelement("AirFreightCheckbox").click();
	}
	
	@Test (priority = 6)
	public void verifyStandardDeliveryFilter() throws IOException, InterruptedException {
	//verifyStandardDeliveryType
	findelement("StandardCheckbox").click();
	verifyCargosService("Dubai Cargos","Personal Effects","Standard","Air Freight","Door To Door","1-1 Days","","AED 1", "AED 0 per kg");
	findelement("StandardCheckbox").click();
	}
	
	@Test (priority = 7)
	public void verifyExpressDeliveryFilter() throws IOException, InterruptedException {
	//verifyExpressDeliveryType	
	findelement("ExpressCheckbox").click();
	verifyCargosService("Dubai Cargos","Personal Effects","Express","Air Freight","Door To Door","1-1 Days","","AED 1", "AED 0 per kg");
	findelement("ExpressCheckbox").click();
	}
	
	@Test (priority = 8)
	public void verifyDoorToDoorDeliveryFilter() throws IOException, InterruptedException {
	//verifyDoorToDoorDeliveryType
	findelement("DoorToDoorCheckbox").click();
	verifyCargosService("Dubai Cargos","Personal Effects","Standard","Air Freight","Door To Door","1-1 Days","","AED 1", "AED 0 per kg");
	findelement("DoorToDoorCheckbox").click();
}
	
	@Test (priority = 9)
	public void verifyDoorToPortDeliveryFilter() throws IOException, InterruptedException {
	//verifyDoorToPortDeliveryType		
	findelement("DoorToPortCheckbox").click();
	verifyCargosService("Dubai Cargos","Personal Effects","Standard","Air Freight","Door To Door","Door To Port","","AED 1", "AED 0 per kg");
	findelement("DoorToPortCheckbox").click();
	}
	
	@Test (priority = 10)
	public void selectCargo() throws IOException, InterruptedException {
	
	selectCargosService("Dubai Cargos","Personal Effects","Standard","Air Freight","Door To Door","1-1 Days","","AED 1", "AED 0 per kg");
	findelement("Book Now").click();
	Thread.sleep(3000);
	}
	
	@Test (priority = 11)
	public void enterSenderInfo() throws IOException, InterruptedException {
	enterSendersInfo("Swapnil","234577889","asd@shyft.com","15 August","08:00 AM - 09:00 AM","Kondapur, hyderabad","Dubai");
	}
	
	@Test (priority = 12)
	public void enterRecieverInfo()throws IOException, InterruptedException {
	enterRecieversInfo("Singhai","57467494","qwerty@shyft.com","Tikamgarh","The Dubai mall");
	Thread.sleep(3000);
	}
	
	@Test (priority = 13)
	public void ConfirmnPay() throws IOException, InterruptedException{
		findelement("Cash on Cargo PickUp").click();
		Thread.sleep(3000);
		findelement("Confirm & Pay").click();
	}
	
	@Test (priority = 14)
	public void BookingConfirmation() throws IOException, InterruptedException{
		verifyAttribute("BookingConfirmation","text","Your booking has been placed");
		verifyAttribute("AgentConfirmation","text","One of our agents will be in touch with you as soon as possible.");
		verifyAttribute("BoookingID","text","Booking Id: SHFT");
		verifyAttribute("BoookingStatus","text","Status: Booking Created");
		verifyAttribute("ConfirmationOrigin","text","DXB, Dubai, United Arab Emirates");
		verifyAttribute("ConfirmationPickupName","text","Swapnil");
		verifyAttribute("ConfirmationPickupAddress","text","Kondapur, hyderabad");
		verifyAttribute("ConfirmationPickupPhNum","text","+234577889");
		verifyAttribute("ConfirmationDestination","text","CMB, Colombo, Sri Lanka");
		verifyAttribute("ConfirmationDeliveryName","text","Singhai");
		verifyAttribute("ConfirmationDeliveryAddress","text","Tikamgarh");
		verifyAttribute("ConfirmationDeliveryPhNum","text","+57467494");
		verifyAttribute("Confirmation Scheduled Pickup","text","15/08/2020 08:00 AM - 09:00 AM");
		verifyAttribute("ConfirmationPaymentMode","text","Collect Cash on Delivery");
		verifyAttribute("ConfirmationMaxWeight","text","123 kg");
		verifyAttribute("ConfirmationTotalPrice","text","AED 1");
		verifyAttribute("ConfirmationPricePerKg","text","AED 0 per kg");
		findelement("Back to Home Page").click();
		Thread.sleep(3000);
		verifyobjectproperties("Origin");
	}
	
	
	
	public void Cargo_setTime(String time) {
//		driver.findElement(By.xpath("//div[@class='timeDropdown ']//ul//li[text()='"+time+"']")).click();
		driver.findElement(By.xpath("//li[contains(text(),'"+time+"')]")).click();
		
	}
	
	public void enterRecieversInfo(String senderName, String mobNum, String emailAddress, String housenum, String location) throws IOException, InterruptedException {
		Thread.sleep(3000);
		findelement("Receiver Name").sendKeys(senderName);
		findelement("Receiver Mobile Number").sendKeys(mobNum);
		findelement("Receiver Email Address").sendKeys(emailAddress);
		findelement("Receiver House Number").sendKeys(housenum);	
		Cargo_setLocation(location);

		Thread.sleep(5000);
		findelement("Continue").click();
	}
	
	public void enterSendersInfo(String senderName, String mobNum, String emailAddress, String date,String pickuptime,String housenum, String location) throws IOException, InterruptedException {
		findelement("Sender Name").sendKeys(senderName);
		findelement("Sender Mobile Number").sendKeys(mobNum);
		findelement("Sender Email Address").sendKeys(emailAddress);
		findelement("Date").click();	
		Cargo_setCalenderDate(date);
		findelement("Pickup Time").click();	
		Thread.sleep(3000);
		Cargo_setTime(pickuptime);
		findelement("Sender House Number").sendKeys(housenum);
		
		Cargo_setLocation(location);

		Thread.sleep(5000);
		findelement("Continue").click();
	}
	
	public void Cargo_setLocation(String location) throws IOException, InterruptedException {
		Thread.sleep(10000);
		findelement("Search Location").sendKeys(location);
		Thread.sleep(5000);

		driver.findElement(By.xpath("//div[@class='pac-container pac-logo hdpi']//div[@class='pac-item'][1]")).click();		
					
	}
	
	public void verifyCargosService(String v_CargoName,String v_Cargotype,String v_shippingType,String v_shippingMethod,String v_clearanceType,String v_deliverytime,String v_MaxWeigth,String v_TotalPrice, String v_PPrice) throws InterruptedException {
//		Webcard = //div[@class='service']
		
		Thread.sleep(5000);
		
		String resultCnt = driver.findElement(By.xpath("//div[@class='results']//h3")).getText();
		System.out.println(resultCnt);
		if (!resultCnt.contentEquals("0 Results Found")) {
		
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			
		Thread.sleep(5000);
				List<WebElement> CargoList = driver.findElements(By.xpath("//div[@class='service']//div[@class='name']//h2"));
				int CargoNum = 0;
				System.out.println(CargoList.size());
				for (WebElement Cargo:CargoList)
				{
				String CargoName = Cargo.getText();
				if (CargoName.contentEquals(v_CargoName)) {
					 CargoNum = CargoList.indexOf(Cargo);
					 CargoNum = CargoNum+1;
					 js.executeScript("arguments[0].scrollIntoView(false);", Cargo);
					break;
				}
				
				
				}
				WebElement obj_block = driver.findElement(By.xpath("(//div[@class='service'])"+"["+ CargoNum+"]" ));
			
			String CargoName = obj_block.findElement(By.xpath("//div[@class='name']//h2")).getText();
			verifyValue("CargoName" ,CargoName, v_CargoName);
			String cargotype=obj_block.findElement(By.xpath("//div[@class='name']//b[text()='Personal Effects']")).getText();
			verifyValue("CargoName" ,cargotype, v_Cargotype);
			String shippingType=obj_block.findElement(By.xpath("//div[@class='companyServices']//span[text()='Standard']")).getText();
			verifyValue("CargoName" ,shippingType, v_shippingType);
			String shippingMethod=obj_block.findElement(By.xpath("//div[@class='companyServices']//span[contains(@class,'btn freight')]")).getText();
			verifyValue("CargoName" ,shippingMethod, v_shippingMethod);
			String clearanceType=obj_block.findElement(By.xpath("//div[@class='companyServices']//span[contains(@class,'btn droptype')]")).getText();
			verifyValue("CargoName" ,clearanceType, v_clearanceType);
			String deliverytime = obj_block.findElement(By.xpath("//div[@class='companyServices']//span[@class='btn days' and contains(.,'Days')]")).getText();
			verifyValue("CargoName" ,deliverytime, v_deliverytime);
			String MaxWeigth = obj_block.findElement(By.xpath("//div[@class='priceBlock']//h3[contains(.,'kg')]")).getText();
			verifyValue("CargoName" ,MaxWeigth, v_MaxWeigth);
			String TotalPrice=obj_block.findElement(By.xpath("//div[@class='priceBlock']//h3[contains(.,'AED')]")).getText();
			verifyValue("CargoName" ,TotalPrice, v_TotalPrice);
			String PPrice = obj_block.findElement(By.xpath("//div[@class='priceBlock']//span[contains(.,'per kg')]")).getText();
			verifyValue("CargoName" ,PPrice, v_PPrice);
//			WebElement viewDetails = obj_block.findElement(By.xpath("//button[text()='View Details ']"));
//			obj_block.click();
		}
		else
		{
			ExtentTestManager.getTest().log(Status.FAIL,"0 Results Found");
		}
	}
	
	public void selectCargosService(String v_CargoName,String v_Cargotype,String v_shippingType,String v_shippingMethod,String v_clearanceType,String v_deliverytime,String v_MaxWeigth,String v_TotalPrice, String v_PPrice) throws InterruptedException {
//		Webcard = //div[@class='service']
		
		Thread.sleep(5000);
		
		String resultCnt = driver.findElement(By.xpath("//div[@class='results']//h3")).getText();
		System.out.println(resultCnt);
		if (!resultCnt.contentEquals("0 Results Found")) {
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	
		Thread.sleep(5000);
				List<WebElement> CargoList = driver.findElements(By.xpath("//div[@class='service']//div[@class='name']//h2"));
				int CargoNum = 0;
				System.out.println(CargoList.size());
				for (WebElement Cargo:CargoList)
				{
				String CargoName = Cargo.getText();
				if (CargoName.contentEquals(v_CargoName)) {
					 CargoNum = CargoList.indexOf(Cargo);
					 CargoNum = CargoNum+1;
					 
					break;
				}
				js.executeScript("arguments[0].scrollIntoView(false);", Cargo);
				
				}
				WebElement obj_block = driver.findElement(By.xpath("(//div[@class='service'])"+"["+ CargoNum+"]" ));
				
			String CargoName = obj_block.findElement(By.xpath("//div[@class='name']//h2")).getText();
			verifyValue("CargoName" ,CargoName, v_CargoName);
			String cargotype=obj_block.findElement(By.xpath("//div[@class='name']//b[text()='Personal Effects']")).getText();
			verifyValue("CargoName" ,cargotype, v_Cargotype);
			String shippingType=obj_block.findElement(By.xpath("//div[@class='companyServices']//span[text()='Standard']")).getText();
			verifyValue("CargoName" ,shippingType, v_shippingType);
			String shippingMethod=obj_block.findElement(By.xpath("//div[@class='companyServices']//span[contains(@class,'btn freight')]")).getText();
			verifyValue("CargoName" ,shippingMethod, v_shippingMethod);
			String clearanceType=obj_block.findElement(By.xpath("//div[@class='companyServices']//span[contains(@class,'btn droptype')]")).getText();
			verifyValue("CargoName" ,clearanceType, v_clearanceType);
			String deliverytime = obj_block.findElement(By.xpath("//div[@class='companyServices']//span[@class='btn days' and contains(.,'Days')]")).getText();
			verifyValue("CargoName" ,deliverytime, v_deliverytime);
			String MaxWeigth = obj_block.findElement(By.xpath("//div[@class='priceBlock']//h3[contains(.,'kg')]")).getText();
			verifyValue("CargoName" ,MaxWeigth, v_MaxWeigth);
			String TotalPrice=obj_block.findElement(By.xpath("//div[@class='priceBlock']//h3[contains(.,'AED')]")).getText();
			verifyValue("CargoName" ,TotalPrice, v_TotalPrice);
			String PPrice = obj_block.findElement(By.xpath("//div[@class='priceBlock']//span[contains(.,'per kg')]")).getText();
			verifyValue("CargoName" ,PPrice, v_PPrice);
			WebElement viewDetails = obj_block.findElement(By.xpath("//button[text()='View Details ']"));
			obj_block.click();
		}
		else
		{
			ExtentTestManager.getTest().log(Status.FAIL,"0 Results Found");
		}
	}
	
	@Test
	@DataProvider(name="testdata")
	public Object[][] testDataExample(){
	ReadExcelFile configuration = new ReadExcelFile("C:\\Users\\jains\\OneDrive\\Desktop\\Shyft\\ShyftBaggageBookingTestData.xlsx");
	int rows = configuration.getRowCount(0);
	Object[][]signin_credentials = new Object[rows][2];

	for(int i=0;i<rows;i++)
	{
	signin_credentials[i][0] = configuration.getData(0, i, 0);
	System.out.println(signin_credentials[i][0]);
	signin_credentials[i][1] = configuration.getData(0, i, 1);
	System.out.println(signin_credentials[i][1]);
	}
	return signin_credentials;
	}
	
	public void Cargo_setCalenderDate(String s_Date) throws InterruptedException {
		String c_Date = s_Date.split(" ")[0];
		String c_Month = s_Date.split(" ")[1];
		String getmonth = driver.findElement(By.xpath("//div[@class='react-datepicker__current-month']")).getText().split(" ")[0];
		System.out.println(getmonth);
		while (!getmonth.equalsIgnoreCase(c_Month)) {					
			if (!getmonth.equalsIgnoreCase(c_Month)) {
				System.out.println(getmonth);
				System.out.println(c_Month);
				driver.findElement(By.xpath("//button[@class='react-datepicker__navigation react-datepicker__navigation--next']")).click();
				Thread.sleep(3000);
				getmonth = driver.findElement(By.xpath("//div[@class='react-datepicker__current-month']")).getText().split(" ")[0];		
			}
		}
		Thread.sleep(3000);
		driver.findElement(By.xpath("//div[text() = '" + c_Date + "']")).click();
		
	}
	public void selectcombobox(String val,String input_ele,String options_ele) throws IOException, InterruptedException {
		String readonly = findelement(input_ele).getAttribute("readonly");
		findelement(input_ele).click();
		System.out.println(readonly);
		if (readonly==null)
		{findelement(input_ele).clear();
		findelement(input_ele).sendKeys(Keys.HOME,Keys.chord(Keys.SHIFT,Keys.END),val);
		}
		
		Thread.sleep(3000);
		List<WebElement> list1 = findelement(options_ele).findElements(By.xpath("//a|//li"));
		for (WebElement ele :list1){
			String actual =ele.getText();
			if (actual.contains(val)) {
				ele.click();
			System.out.println(actual);
			break;
			}
		}
//		findelement("//input[@placeholder='Select Origin']/parent::div//following-sibling::div[@aria-label='menu-options']").click();
		
	}
	
	public void verifyAlertMsg(String expected_msg) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'fade fixed-top alert')]")));
		String actual_msg = driver.findElement(By.xpath("//div[contains(@class,'fade fixed-top alert')]")).getText();
		if (actual_msg.contains(expected_msg)) {
		ExtentTestManager.getTest().log(Status.PASS, actual_msg + " :is verified");
		}
		else {
			ExtentTestManager.getTest().log(Status.FAIL, "Actual Msg = " +actual_msg+ " ;Expected msg = " + expected_msg );
		}
	}
	
	public void verifyValue(String fieldName, String actual, String expected) {
		if (!expected.isEmpty()) {
		if (actual.contentEquals(expected) ) {
			ExtentTestManager.getTest().log(Status.PASS,fieldName + " : " + expected + " is verified");			
		}
		else
		{
			ExtentTestManager.getTest().log(Status.FAIL, fieldName + " : " + " expected value = " +expected + System.lineSeparator() +" ;\r\nactual value = "+ actual);		
		}
		}
	}
	
	public void verifyobjectproperties( String v_ele , String MsgPass , String MsgFail  ) throws IOException {
		
		if (findelement(v_ele).isDisplayed()) {
			ExtentTestManager.getTest().log(Status.PASS, MsgPass);
		}
			else {
				ExtentTestManager.getTest().log(Status.FAIL, MsgFail);	
			}
		}
	
	public void verifyobjectproperties( String v_ele) throws IOException {
		
		if (findelement(v_ele).isDisplayed()) {
			ExtentTestManager.getTest().log(Status.PASS, v_ele + " is displayed");
		}
			else {
				ExtentTestManager.getTest().log(Status.FAIL, v_ele + " is not displayed");	
			}
		}

	public void verifyAttribute (String a_path, String attr, String expected) throws IOException {
		WebElement a_ele = findelement(a_path);

		String actual =  a_ele.getAttribute(attr);
		if ( !(actual==null)){
			if  (attr.equalsIgnoreCase("text")) {
				actual = a_ele.getText();
			}
			System.out.println(actual);
			
			if (actual.contentEquals(expected) ) {
				ExtentTestManager.getTest().log(Status.PASS,a_path + " : " + expected + attr + " is verified");			
			}
			else
			{
				ExtentTestManager.getTest().log(Status.FAIL, a_path + " : " + " expected value = " +expected + System.lineSeparator() +" ;\r\nactual value = "+ actual);		
			}
		}
		else
		{
			if  (attr.equalsIgnoreCase("text")) {
				actual = a_ele.getText();
			}
			if (actual.contains(expected) ) {
				ExtentTestManager.getTest().log(Status.PASS,a_path + " : " + expected + attr + " is verified");			
			}
			else
			{
				ExtentTestManager.getTest().log(Status.FAIL, a_path + " : " + " expected value = " +expected + System.lineSeparator() +" ;\r\nactual value = "+ actual);		
			}
			
		}

	}
	
	public void verifyAttribute (String a_path, String e_name, String attr, String expected) throws IOException {
		WebElement a_ele = findelement(a_path);
		String actual =  a_ele.getAttribute(attr);
		if (!actual.isEmpty()){
			if (attr.equalsIgnoreCase("text")) {
				actual = a_ele.getText();
			}
			System.out.println(actual);
			
			if (actual == expected ) {
				ExtentTestManager.getTest().log(Status.PASS,e_name + " : " + expected + attr + "is verified");			
			}
			else
			{
				ExtentTestManager.getTest().log(Status.FAIL, e_name + " : " + "expected value = " +expected + System.lineSeparator() +" \r\nactual value = "+ actual );		
			}
		}
		else
		{
			ExtentTestManager.getTest().log(Status.FAIL, e_name + " : " + " expected value = is null ");
		}
	}
	
	public WebElement findelement(String e_path) throws IOException {
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObjpath(e_path))));
		WebElement e_obj = driver.findElement(By.xpath(getObjpath(e_path)));
		if (e_path.contentEquals("")) {
			e_obj = driver.findElement(By.xpath(e_path));
		}
		
		
		return e_obj;
	}
	
		
	    public String getObjpath(String objName) throws IOException {

	    	String objLocator = null;
			 JSONParser parser = new JSONParser();
			try {
				Object obj = parser.parse(new FileReader(System.getProperty("user.dir") + orName));
	 			
				JSONObject jsonObject = (JSONObject) obj;
	 
//				// A JSON array. JSONObject supports java.util.List interface.
				JSONArray PageObj = (JSONArray) jsonObject.get("Signup");
				for(int i=0;i<PageObj.size();i++){ 
					JSONObject jsonchildObject = (JSONObject) PageObj.get(i);
				
					if (jsonchildObject.get("Object Name").toString().contentEquals(objName)) {
						System.out.println(jsonchildObject.get("Locator").toString());
						objLocator = (jsonchildObject.get("Locator").toString()).split("\\:",2)[1];
						break;
						}
				}

				}
			
			catch (Exception e) {
				e.printStackTrace();
			}
			return objLocator;

	    } 
		
	
	@BeforeTest
	public void beforeTest() {	
		//Create prefs map to store all preferences 
		Map<String, Object> prefs = new HashMap<String, Object>();

		//Put this into prefs map to switch off browser notification (2 to disable)
		prefs.put("profile.default_content_setting_values.notifications", 1);

		//Create chrome options to set this xdjjprefs
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);
		System.setProperty("webdriver.chrome.driver","C:\\Selenium Webdrivers\\chromedriver.exe");
		 driver = new ChromeDriver(options);
		 wait = new WebDriverWait(driver,300);

		
	}	
	
}
