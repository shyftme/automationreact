package example;		

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import static org.testng.AssertJUnit.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.By;		
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.Status;
import com.beust.jcommander.Parameter;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import TestData.ReadExcelFile;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;	
import example.ExtentReporterNG;

@SuppressWarnings("unchecked")
public class NewTest  {		

	
//	private static final String String = null;
	private WebDriver driver;	
	private WebDriverWait wait;
	private SoftAssert softassert;
	private String pickupdetails, dropoffdetails;
	public String orName = "/src/main/resources/ObjectRepository.json" ;

	
//	private File jsonfile;	    
//	Object obj = parser.parse(new FileReader(System.getProperty("user.dir")+"src/main/resources/ObjectRepository.json"));
//	 @BeforeSuite
//     public void config()
//     {
//		 }

	
	public void openWebsite(String URL) throws InterruptedException {
		driver.get(URL);  
		Thread.sleep(3000);
//		driver.findElement(By.xpath("//div[@class='bagdrop bf-cont active']//button[contains(@class,'btn btn-primary')]")).click();
//		Thread.sleep(3000);
		driver.manage().window().maximize();
		
		//save the reference to the current window.
//		String parentWindow= driver.getWindowHandle();
//		//switch to new window
//		for (String winHandle : driver.getWindowHandles()) {
//		    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
//		}
	}
	
			@Test(dataProvider="testdata",priority = 0)
		public void BookingFlow(String username, String password) throws InterruptedException, IOException {	


				openWebsite("https://uat.shyftclub.com/login");
			signin(username,password);
			
//			selectLocation();
//			
//			setLocation("Kondapur");
//			
			verifyLocation("No");
			
			selectLocation();
			
			setLocation("The Dubai mall");
			
			Thread.sleep(3000);
			
			verifyLocation("No");
			
			enterFlightDetails("EK1", "1 September", "07:45 AM");
			
			enterPickUpDetails("31 August", "11:45 AM","345","13",3);
			
			enterDropoffDetails("1 September", "4:45 AM");
			
			enterBookingSummary("Promo1");
			verifyThankYouPage() ;
			Thread.sleep(5000);
			verifyBookingDetails("", "" ,"", "345,13,Downtown Dubai - Dubai - United Arab Emirates","Dubai Airport Terminal 3");
			 
		}	
		
		public void verifyAlertMsg(String expected_msg) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'fade fixed-top alert')]")));
			String actual_msg = driver.findElement(By.xpath("//div[contains(@class,'fade fixed-top alert')]")).getText();
			if (actual_msg.contains(expected_msg)) {
			ExtentTestManager.getTest().log(Status.PASS, actual_msg + " :is verified");
			}
			else {
				ExtentTestManager.getTest().log(Status.FAIL, "Actual Msg = " +actual_msg+ " ;Expected msg = " + expected_msg );
			}
		}
			
		    public String getObjpath(String objName) throws IOException {
//				jsonfile = new File(System.getProperty("user.dir")+"/src/main/resources/ObjectRepository.json");
//				System.out.println(JsonPath.read(jsonfile, "$."+"Signup.Email.Locator"));
		    	String objLocator = null;
				 JSONParser parser = new JSONParser();
				try {
					Object obj = parser.parse(new FileReader(System.getProperty("user.dir") + orName));
		 
					// A JSON object. Key value pairs are unordered. JSONObject supports java.util.Map interface.
				
					JSONObject jsonObject = (JSONObject) obj;
		 
//					// A JSON array. JSONObject supports java.util.List interface.
					JSONArray PageObj = (JSONArray) jsonObject.get("Signup");
					for(int i=0;i<PageObj.size();i++){ 
						JSONObject jsonchildObject = (JSONObject) PageObj.get(i);
					
//					JSONObject Objectnew = (JSONObject) jsonchildObject.get("Object Name");
					//
						if (jsonchildObject.get("Object Name").toString().contentEquals(objName)) {
							System.out.println(jsonchildObject.get("Locator").toString());
							objLocator = (jsonchildObject.get("Locator").toString()).split("\\:",2)[1];
							break;
							}
//							else
//							{System.out.println(jsonchildObject.get("Object Name").toString());
//							 objLocator = jsonchildObject.get("Object Name").toString();
//							}

					}
//					JSONArray objectList = (JSONArray) jsonchildObject.get("Object Name");
//						
//		 
					// An iterator over a collection. Iterator takes the place of Enumeration in the Java Collections Framework.
					// Iterators differ from enumerations in two ways:
					// 1. Iterators allow the caller to remove elements from the underlying collection during the iteration with well-defined semantics.
					// 2. Method names have been improved.
//					Iterator<JSONObject> iterator = objectList.iterator();
//					while (iterator.hasNext())
//					for(int i=0;i<jsonchildObject.size();i++){
//						if (jsonchildObject.toString().contentEquals("objName")) {
//						System.out.println(jsonObject.get("Locator"));
//						}
//					}
					}
				
				catch (Exception e) {
					e.printStackTrace();
				}
				return objLocator;
					
				
		
//				
//		        return returnValue;
		    } 
			
//		   @Parameter ({"incorrectusername","incorrectpassword"})
		public void signin(String Username, String Password) throws IOException {

			
			findelement("Emailid").sendKeys(Username);
			findelement("password").sendKeys(Password);
			findelement("submit").click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObjpath("LuggageText")))).isDisplayed();
			
			ExtentTestManager.getTest().log(Status.PASS, "LuggageText is verified");
			
			
		}
		
		@Test (priority = 1)
		public void verifyHamburger_options() throws IOException, InterruptedException {
			Thread.sleep(3000);
			findelement("HamburgerIcon").click();
			verifyobjectproperties("Home");			
			verifyobjectproperties("Notifications");
			verifyobjectproperties("My Booking");
			verifyobjectproperties("My Account");
			verifyobjectproperties("Pricing");
			verifyobjectproperties("Contact Us");
			verifyobjectproperties("Sign out");
		}
		
		@Test (priority = 2)
		public void verifyHamburger_Bookings() throws IOException, InterruptedException {
			Thread.sleep(3000);
			findelement("My Booking").click();
			Thread.sleep(5000);
			verifyBookingDetails("DXB2007200003", "Fri, 14 Aug 03:45 - 04:15" ,"Fri, 14 Aug 04:45 - 05:15", "345,13,Downtown Dubai - Dubai - United Arab Emirates","Dubai Airport Terminal 3");			
		}
		
		@Test (priority = 3)
		public void verifyHamburger_MyAccount() throws IOException, InterruptedException {
			Thread.sleep(3000);
			findelement("Backarrow").click();
			findelement("HamburgerIcon").click();
			Thread.sleep(3000);
			findelement("My Account").click();
			Thread.sleep(3000);
//			verifyAttribute("User Name","text","Swapnil Jain");
//			verifyAttribute("User id","text","jainswapnil199@gmail.com");
			
			verifyobjectproperties("Personal details");
			verifyobjectproperties("Change Password");
			verifyobjectproperties("Terms & Conditions");
			driver.findElement(By.xpath("//div[@class='userInfo']//h4[@class='mt-4']")).isDisplayed();
			String actual =driver.findElement(By.xpath("//div[@class='userInfo']//h4[@class='mt-4']")).getText();
			System.out.println(actual);
//			actual =driver.findElement(By.xpath("//div[@class='userInfo']//h4[@class='mt-4']")).getAttribute("textContent");
//			System.out.println(actual);
//			actual =driver.findElement(By.xpath("//div[@class='userInfo']//h4[@class='mt-4']")).getAttribute("innertext");
//			System.out.println(actual);	
//			
			
			softassert.assertEquals(actual,"Swapnil Jain","User name is not correct");		
			driver.findElement(By.xpath("//div[@class='userInfo']//span[@class='text-muted']")).isDisplayed();
			softassert.assertEquals(driver.findElement(By.xpath("//div[@class='userInfo']//span[@class='text-muted']")).getText(),"jainswapnil199@gmail.com");
			softassert.assertAll();
		}
		
		@Test (priority = 4)
		public void verifyHamburger_MyAccount_Personaldetails() throws IOException, InterruptedException {
			Thread.sleep(3000);

			
			findelement("Personal details").click();
			Thread.sleep(3000);
			verifyAttribute("fullName","value","Swapnil Jain");
			verifyAttribute("email","value","jainswapnil199@gmail.com");
			verifyAttribute("mobileNumber","value","8886499937");
//			softassert.assertEquals(findelement("country").getText(),"Swapnil Jain");
			verifyAttribute("mobileNumber","value","8886499937");
//			softassert.assertEquals(findelement("mobileNumber").getText(),"8886499937");
			findelement("fullName").clear();
			findelement("fullName").sendKeys("Swapnil Jain");
			findelement("submit_Update").click();
			verifyAlertMsg("We have recieved your message. Thank You");
			Thread.sleep(3000);
			findelement("fullName").clear();
			findelement("fullName").sendKeys("Demo");
			findelement("submit_Update").click();
		}
		
		@Test (priority = 5)
		public void verifyHamburger_MyAccount_ChangePwd() throws IOException, InterruptedException {
			Thread.sleep(5000);
			findelement("Backarrow").click();
			findelement("HamburgerIcon").click();
			Thread.sleep(3000);
//			verifyAttribute("User Name_Hamburger","text","Swapnil Jain");
//			verifyAttribute("User id_Hamburger","text","jainswapnil199@gmail.com");
			softassert.assertEquals(findelement("User Name_Hamburger").getText(),"Swapnil Jain");
			softassert.assertEquals(findelement("User id_Hamburger").getText(),"jainswapnil199@gmail.com");
			Thread.sleep(3000);
			findelement("My Account").click();
			findelement("Change Password").click();
			findelement("OldPassword").sendKeys("shyft456");
			findelement("NewPassword").sendKeys("shyft456");
			findelement("ConfirmPassword").sendKeys("shyft456");
			findelement("submit_Update").click();
			verifyAlertMsg("We have recieved your message. Thank You");
			
		}
		
		@Test (priority = 6)
		public void verifyHamburger_MyAccount_TermsnConditions() throws IOException, InterruptedException {
			Thread.sleep(5000);
			findelement("HamburgerIcon").click();
			Thread.sleep(3000);
			findelement("My Account").click();
			Thread.sleep(3000);	
			findelement("Terms & Conditions").click();
			findelement("Backarrow").click();
			
		}
		
		@Test (priority = 7)
		public void verifyHamburger_MyAccount_Pricing() throws IOException, InterruptedException {
			Thread.sleep(3000);
			findelement("Backarrow").click();
			findelement("HamburgerIcon").click();
			Thread.sleep(3000);
			findelement("Pricing").click();
		
		}
		
		@Test (priority = 8)
		public void verifyHamburger_ContactUs() throws IOException, InterruptedException {
			Thread.sleep(3000);
			findelement("Backarrow").click();
			findelement("HamburgerIcon").click();
			findelement("Contact Us").click();
			Thread.sleep(3000);
			verifyAttribute("FullName_ContactUs","value","Swapnil Jain");
			verifyAttribute("Email_ContactUs","value","jainswapnil199@gmail.com");
			verifyAttribute("PhoneNumber_ContactUs","value","+918886499937");
			findelement("Message").sendKeys("Mymsg");
			Thread.sleep(3000);
			
			findelement("submit_ContactUs").click();
			verifyAlertMsg("We have recieved your message. Thank You");
			Thread.sleep(3000);
			findelement("close_alert").click();
		}
		
		@Test (priority = 10)
		public void verifyHamburger_Signout() throws IOException, InterruptedException {
			Thread.sleep(5000);
			findelement("Backarrow").click();
			findelement("HamburgerIcon").click();
			findelement("Sign out").click();
			
		}
		
		
		public void selectLocation() throws IOException, InterruptedException {
			WebElement title = driver.findElement(By.xpath("//div//h4[text()='Where do we pick up your luggage']"));
	         String expectedText = "Where do we pick up your luggage";
	         String originalText = title.getText();
	         verifyValue("luggage text", originalText, expectedText);
	         if (findelement("MenuIcon").isDisplayed()) {
					ExtentTestManager.getTest().log(Status.PASS, "Menu Icon is displayed");
				}
					else {
						ExtentTestManager.getTest().log(Status.FAIL, "Menu Icon is not displayed");	
					}
		
				if (findelement("StepNav1").isDisplayed()) {
					ExtentTestManager.getTest().log(Status.PASS,"StepNav1 is displayed" );
				}
					else {
						ExtentTestManager.getTest().log(Status.FAIL, "StepNav1 is not displayed");	
					}

	         Thread.sleep(5000);
	         wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='liAddress']//span[@class='text-truncate d-block']")));
	         driver.findElement(By.xpath("//div[@class='liAddress']//span[@class='text-truncate d-block']")).click();
//	         Reporter.setCurrentTestResult(m);
		}
		
		public void setLocation(String location) throws IOException, InterruptedException {
			Thread.sleep(10000);
			findelement("Location").sendKeys(location);
			Thread.sleep(5000);
			driver.findElement(By.xpath("//div[@class='pac-container pac-logo hdpi'][1]")).click();
						
		}
		
		public void verifyLocation(String serviceable) throws IOException {
			String buttonprop = findelement("ButtonEnterFlightDetails").getAttribute("class");
			System.out.println(buttonprop);
			if (serviceable.equalsIgnoreCase("No")) {
				if (buttonprop.contains("disableBtnGrey")) {
					ExtentTestManager.getTest().log(Status.PASS,"verified area is not serviceable" );
					}
				else {
					ExtentTestManager.getTest().log(Status.FAIL,"area is serviceable" );	
					}}
			else {
				ExtentTestManager.getTest().log(Status.PASS,"verified area is serviceable" );
					}
					
		}
		
		public void enterFlightDetails(String FlightNumber, String date, String time) throws IOException, InterruptedException {
			findelement("ButtonEnterFlightDetails").click();
			findelement("InputFlightNumber").sendKeys(FlightNumber);
			findelement("InputFlightDate").click();
			Thread.sleep(3000);
			setCalenderDate(date);
			Thread.sleep(3000);
//			findelement("CalenderIconOk").click();
			Thread.sleep(3000);
			verifyAttribute("InputFlightTime","value",time);
				         
		}
		
		public void enterPickUpDetails(String PickupDate, String PickupTime,String HomeNum,String FloorNum,int LuggageCount) throws IOException, InterruptedException {
			findelement("ButtonLuggagepickupdetails").click();
			Thread.sleep(3000);
			findelement("InputPickupDate").click();	
			//	"Locator": "XPath://label[text()='Pickup Date']//parent::div//div[@class='MuiFormControl-root MuiTextField-root']"			
			setCalenderDate(PickupDate);
//			findelement("CalenderIconOk").click();
//			findelement("InputPickupTime").click();	
			Thread.sleep(3000);
//			driver.(By.xpath("//div[@class='MuiPickersClockPointer-pointer MuiPickersClockPointer-animateTransformn']"))
			setTime(PickupTime);
			
			//"Locator": "XPath://label[text()='Pickup Time']//parent::div/div[@class='MuiFormControl-root MuiTextField-root']"		
//			driver.findElement(By.xpath("//span[contains(@class,'MuiTypography-body1') and text()='7']")).click();
//			findelement("CalenderIconOk").click();

			String getBaggageCount = findelement("BaggageCount").getAttribute("value");
			int ibaggageCount = Integer.parseInt(getBaggageCount);
			
			findelement("HouseNumber").sendKeys(HomeNum);
			findelement("FloorNumber").sendKeys(FloorNum);
			
			while (ibaggageCount != LuggageCount) {
			driver.findElement(By.xpath("//button[contains(@class,'btn btn-secondary btn-sm') and text()='+']")).click();			
			getBaggageCount = findelement("BaggageCount").getAttribute("value");
			ibaggageCount = Integer.parseInt(getBaggageCount);		
			
			pickupdetails = PickupDate + PickupTime;
			}
		}
		
		public void setCalenderDate(String s_Date) throws InterruptedException {
			String flightDate = s_Date.split(" ")[0];
			String flightMonth = s_Date.split(" ")[1];
//			String getmonth = driver.findElement(By.xpath("//p[contains(@class,'MuiTypography-root MuiTypography-body1 MuiTypography-alignCenter')]")).getText().split(" ")[0];
			String getmonth = driver.findElement(By.xpath("//div[contains(@class,'react-datepicker__current-month')]")).getText().split(" ")[0];
			
			System.out.println(getmonth);
			while (!getmonth.equalsIgnoreCase(flightMonth)) {					
				if (!getmonth.equalsIgnoreCase(flightMonth)) {
					System.out.println(getmonth);
					System.out.println(flightMonth);
					driver.findElement(By.xpath("//button[@class='react-datepicker__navigation react-datepicker__navigation--next']")).click();
					Thread.sleep(3000);
					getmonth = driver.findElement(By.xpath("//div[contains(@class,'react-datepicker__current-month')]")).getText().split(" ")[0];				
				}
			}
			Thread.sleep(3000);
			driver.findElement(By.xpath("//p[text() = '" + flightDate + "']")).click();
			
		}
		
		public void setTime(String s_Time) throws InterruptedException {
//			String s_hour = s_Time.split(":")[0];
//			String s_min = s_Time.split(":")[1];
//			String s_min1 = s_min.split(" ")[0];
//			String s_zone = s_Time.split(" ")[1];
//			WebElement sourceLocatorhr = driver.findElement(By.xpath("//span[contains(@class,'MuiPickersClockNumber-clockNumberSelected')]"));
//			WebElement targetLocatorhr = driver. findElement(By.xpath("//span[contains(@class,'MuiTypography-body1') and text()='"+ s_hour +"']"));
//			Actions action = new Actions(driver);
//			action. dragAndDrop(sourceLocatorhr, targetLocatorhr). build(). perform();
//			Thread.sleep(5000);
////			WebElement sourceLocatormin = driver.findElement(By.xpath("//span[contains(@class,'MuiPickersClockPointer-thumb MuiPickersClockPointer-noPoint')]"));
//			WebElement sourceLocatormin = driver.findElement(By.xpath("//div[contains(@class,'MuiPickersClockPointer-thumb')]"));
//			WebElement targetLocatormin = driver. findElement(By.xpath("//span[contains(@class,'MuiTypography-body1') and text()='"+ s_min1 +"']"));
//			action. dragAndDrop(sourceLocatormin, targetLocatormin). build(). perform();
//			Thread.sleep(3000);
//			driver.findElement(By.xpath("//span[@class='MuiButton-label']//h6[contains(@class,'ampmLabel') and text() ='" + s_zone + "']")).click();
			
			driver.findElement(By.xpath("//label[text()='Pickup Time']//parent::div//input")).click();
			driver.findElement(By.xpath("//div[contains(@class,'timeDropdown ')]//li[text()='" + s_Time + "']")).click();
		
		}
		
		public void enterDropoffDetails(String DropOffDate, String DropoffTime) throws IOException, InterruptedException {
			findelement("ButtonLuggageDropoffdetails").click();
			Thread.sleep(3000);
			findelement("InputDropoffDate").click();	
			//	"Locator": "XPath://label[text()='Pickup Date']//parent::div//div[@class='MuiFormControl-root MuiTextField-root']"			
			Thread.sleep(3000);
			setCalenderDate(DropOffDate);
			
			findelement("CalenderIconOk").click();
			Thread.sleep(3000);
			
			findelement("InputDropoffTime").click();	
			Thread.sleep(3000);
			
			setTime(DropoffTime);
			findelement("CalenderIconOk").click();
			
			dropoffdetails = DropOffDate + DropoffTime;

		}
		
		public void enterBookingSummary(String Promocode) throws IOException, InterruptedException {
			findelement("ButtonLuggageDropoffdetails").click();
			Thread.sleep(3000);
			findelement("PromoCode").sendKeys(Promocode);
			findelement("ButtonApply").click();
			Thread.sleep(3000);
			findelement("ButtonLetsShyft").click();
		}
		
		public void verifyBookingDetails(String BookingId, String e_bookingpickupdetails, String e_bookingdropoffdetails, String e_BookingpickupAddress,String e_BookingdropoffAddress ) throws IOException, InterruptedException {
			Thread.sleep(3000);
			if (BookingId.contentEquals("")) {
				BookingId = driver.findElement(By.xpath("//div[@class='bookingBlock whiteBox inactive']//div//strong")).getText();
				}
			
			WebElement bookingblock = driver.findElement(By.xpath("//div//strong[text()='"+BookingId+"']/ancestor::div[@class='bookingBlock whiteBox inactive']"));

//			verifyAttribute(pathbookingblock+"//i[@class='icon-locationoutline']/following-sibling::span[@class='text-primary d-inline-block mb-1']","pickuptime","text",pickupdetails);
//			verifyAttribute(pathbookingblock+"//i[@class='icon-suitcase']/following-sibling::span[@class='text-primary d-inline-block mb-1']","dropofftime","text",dropoffdetails);
//			verifyAttribute(pathbookingblock+"//div[@class='bookingBlock whiteBox inactive']//div[@class='text-right small text-uppercase col-4']","Booking Status","text","PENDING");
//			verifyAttribute(pathbookingblock+"//i[@class='icon-locationoutline']/following-sibling::p\"","Booking Status","text",e_BookingpickupAddress);
//			verifyAttribute(pathbookingblock+"//i[@class='icon-suitcase']/following-sibling::p","Booking Status","text",e_BookingdropoffAddress);
			String bookingpickupdetails=bookingblock.findElement(By.xpath("//i[@class='icon-locationoutline']/following-sibling::span[@class='text-primary d-inline-block mb-1']")).getText();		
			System.out.println(bookingpickupdetails);
			System.out.println(pickupdetails);
			verifyValue("pickuptime" ,bookingpickupdetails, pickupdetails);
			String bookingdropdetails=bookingblock.findElement(By.xpath("//i[@class='icon-suitcase']/following-sibling::span[@class='text-primary d-inline-block mb-1']")).getText();
			System.out.println(bookingdropdetails);
			verifyValue("dropofftime",bookingdropdetails, dropoffdetails);
			System.out.println(dropoffdetails);
			String bookingstatus=bookingblock.findElement(By.xpath("//div[@class='bookingBlock whiteBox inactive']//div[@class='text-right small text-uppercase col-4']")).getText();
			System.out.println(bookingstatus);
			verifyValue("BookingStatus",bookingstatus, "PENDING");
			String bookingpickupAddress=bookingblock.findElement(By.xpath("//i[@class='icon-locationoutline']/following-sibling::p")).getText();
			System.out.println(bookingpickupAddress);
			verifyValue("Pickupaddress",bookingpickupAddress,e_BookingpickupAddress);
			String bookingdropoffAddress=bookingblock.findElement(By.xpath("//i[@class='icon-suitcase']/following-sibling::p")).getText();
			System.out.println(bookingdropoffAddress);
			verifyValue("dropoffaddress",bookingdropoffAddress,e_BookingdropoffAddress );
			
		}
		
		public void verifyValue(String fieldName, String actual, String expected) {
			if (actual.contentEquals(expected) ) {
				ExtentTestManager.getTest().log(Status.PASS,fieldName + " : " + expected + " is verified");			
			}
			else
			{
				ExtentTestManager.getTest().log(Status.FAIL, fieldName + " : " + " expected value = " +expected + System.lineSeparator() +" ;\r\nactual value = "+ actual);		
			}
		}
		
		
		public void verifyThankYouPage() throws IOException, InterruptedException {
			Thread.sleep(3000);
//			verifyAttribute("DriporterAssignedText","text","Your Driporter will be assigned shortly, and will be in touch with you soon before the scheduled pickup time.");
			Thread.sleep(5000);
			findelement("ButtonThankYouDone").click();
			Thread.sleep(5000);
		}
		

		
		public void verifyobjectproperties( String v_ele , String MsgPass , String MsgFail  ) throws IOException {
			
			if (findelement(v_ele).isDisplayed()) {
				ExtentTestManager.getTest().log(Status.PASS, MsgPass);
			}
				else {
					ExtentTestManager.getTest().log(Status.FAIL, MsgFail);	
				}
			}
		
		public void verifyobjectproperties( String v_ele) throws IOException {
			
			if (findelement(v_ele).isDisplayed()) {
				ExtentTestManager.getTest().log(Status.PASS, v_ele + " is displayed");
			}
				else {
					ExtentTestManager.getTest().log(Status.FAIL, v_ele + " is not displayed");	
				}
			}

		public void verifyAttribute (String a_path, String attr, String expected) throws IOException {
			WebElement a_ele = findelement(a_path);

			String actual =  a_ele.getAttribute(attr);
			if ( !(actual==null)){
				if  (attr.equalsIgnoreCase("text")) {
					actual = a_ele.getText();
				}
				System.out.println(actual);
				
				if (actual.contentEquals(expected) ) {
					ExtentTestManager.getTest().log(Status.PASS,a_path + " : " + expected + attr + " is verified");			
				}
				else
				{
					ExtentTestManager.getTest().log(Status.FAIL, a_path + " : " + " expected value = " +expected + System.lineSeparator() +" ;\r\nactual value = "+ actual);		
				}
			}
			else
			{
				ExtentTestManager.getTest().log(Status.FAIL, a_path + " : " + " expected value = is null ");
			}

		}
		
		public void verifyAttribute (String a_path, String e_name, String attr, String expected) throws IOException {
			WebElement a_ele = findelement(a_path);
			String actual =  a_ele.getAttribute(attr);
			if (!actual.isEmpty()){
				if (attr.equalsIgnoreCase("text")) {
					actual = a_ele.getText();
				}
				System.out.println(actual);
				
				if (actual == expected ) {
					ExtentTestManager.getTest().log(Status.PASS,e_name + " : " + expected + attr + "is verified");			
				}
				else
				{
					ExtentTestManager.getTest().log(Status.FAIL, e_name + " : " + "expected value = " +expected + System.lineSeparator() +" \r\nactual value = "+ actual );		
				}
			}
			else
			{
				ExtentTestManager.getTest().log(Status.FAIL, e_name + " : " + " expected value = is null ");
			}
		}
		
		public WebElement findelement(String e_path) throws IOException {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObjpath(e_path))));
			WebElement e_obj = driver.findElement(By.xpath(getObjpath(e_path)));
			if (e_path.contentEquals("")) {
				e_obj = driver.findElement(By.xpath(e_path));
			}
			
			
			return e_obj;
		}
		
		@BeforeTest
		public void beforeTest() {	
	
				  int a[] ={20,3,25,30,15},i,j,n=5;
					

					
					for (i=0;i<n;i++) {
						for (j=i+1;j<n;j++) {
							int count = 1;
							if(a[i]<a[j]) {
								
								int y = a[i]*count;
								System.out.println(y);
								count = count +1;
								}
							
//								else {
//									int y = a[i];
//									
//									System.out.println(y);
//									break;
//								}
//							
						}
					
					}
				
			//Create prefs map to store all preferences 
			Map<String, Object> prefs = new HashMap<String, Object>();

			//Put this into prefs map to switch off browser notification (2 to disable)
			prefs.put("profile.default_content_setting_values.notifications", 1);

			//Create chrome options to set this prefs
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", prefs);
			System.setProperty("webdriver.chrome.driver","C:\\Selenium Webdrivers\\chromedriver.exe");
			 driver = new ChromeDriver(options);
			 wait = new WebDriverWait(driver,300);
//			 softassert = new SoftAssert();
			
		}	
			
		
		@Test
		@DataProvider(name="testdata")
		public Object[][] testDataExample(){
		ReadExcelFile configuration = new ReadExcelFile("C:\\Users\\jains\\OneDrive\\Desktop\\Shyft\\ShyftBaggageBookingTestData.xlsx");
		int rows = configuration.getRowCount(0);
		Object[][]signin_credentials = new Object[rows][2];

		for(int i=0;i<rows;i++)
		{
		signin_credentials[i][0] = configuration.getData(0, i, 0);
		System.out.println(signin_credentials[i][0]);
		signin_credentials[i][1] = configuration.getData(0, i, 1);
		System.out.println(signin_credentials[i][1]);
		}
		return signin_credentials;
		}
		
		@AfterTest
		public void afterTest() {
			driver.quit();	
			
			
		}		
		
	
//		public void verifyValue( String obj ) {
//			if (driver.findElement(By.xpath("//div//h4[text()='Where do we pick up your luggage']")).isDisplayed());
//			{ Reporter.setCurrentTestResult("Pass");
//		}
}	