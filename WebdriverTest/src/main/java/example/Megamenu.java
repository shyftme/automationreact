package example;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import example.NewTest;

public class Megamenu {
	private static final String String = null;
	private WebDriver driver;	
	private WebDriverWait wait;
	NewTest method = new NewTest();
	private SoftAssert softassert;


	@Test
	public void verifyMegamenu_options() throws IOException, InterruptedException {
		Thread.sleep(3000);
		findelement("MegamenuIcon").click();
		findelement("Home").isDisplayed();
		findelement("Notifications").isDisplayed();
		findelement("My Booking").isDisplayed();
		findelement("My Account").isDisplayed();
		findelement("Pricing").isDisplayed();
		findelement("Contact Us").isDisplayed();
		findelement("Sign out").isDisplayed();
	}

	public WebElement findelement(String e_path) throws IOException {
		WebElement e_ele = method.findelement(method.getObjpath(e_path));
		return e_ele;
	}
//	@BeforeTest
//	public void beforeTest() {	
//		//Create prefs map to store all preferences 
//		Map<String, Object> prefs = new HashMap<String, Object>();
//
//		//Put this into prefs map to switch off browser notification (2 to disable)
//		prefs.put("profile.default_content_setting_values.notifications", 1);
//
//		//Create chrome options to set this prefs
//		ChromeOptions options = new ChromeOptions();
//		options.setExperimentalOption("prefs", prefs);
//		System.setProperty("webdriver.chrome.driver","C:\\Selenium Webdrivers\\chromedriver.exe");
//		 driver = new ChromeDriver(options);
//		 wait = new WebDriverWait(driver,300);
//		 softassert = new SoftAssert();
//		
//	}	
	
}
